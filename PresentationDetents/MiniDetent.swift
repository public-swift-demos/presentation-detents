//
//  MiniDetent.swift
//  PresentationDetents
//
//  Created by Ben Johnson on 03/12/2022.
//
// Comments in this document are to supplement the tutorial.
// Code clarity is more important in production code - where you wouldn't litter it with comments. 😇

import SwiftUI

private struct MiniDetent: CustomPresentationDetent {
    
    static func height(in context: Self.Context) -> CGFloat? {
        // Choose the maximum value of either 48 pixels or 15% of the maximum detent area.
        // There's no need to use 'return' as we can use implicit returns instead.
        max(48, context.maxDetentValue * 0.15)
    }
    
}

// Extend `PresentationDetent` to make accessing the types of detents easier, although this isn't required.
extension PresentationDetent {
    
    /// A 'mini' detent which is either 48px or 15% of the detent area, whichever is larger.
    static let mini = Self.custom(MiniDetent.self)
    
}
