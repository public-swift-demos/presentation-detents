//
//  PresentationDetentsApp.swift
//  PresentationDetents
//
//  Created by Ben Johnson on 02/12/2022.
//

import SwiftUI

@main
struct PresentationDetentsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
