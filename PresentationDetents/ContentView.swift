//
//  ContentView.swift
//  PresentationDetents
//
//  Created by Ben Johnson on 02/12/2022.
//

import SwiftUI

struct ContentView: View {
    
    @State var showSheet = false
    
    var body: some View {
        ZStack {
            MapView()
            VStack {
                Spacer()
                PressMeButtonView(action: {
                    showSheet.toggle()
                })
                .padding(.bottom, 32)
            }
        }
        .sheet(isPresented: $showSheet) {
            Text("I'm some lovely text")
                .presentationDetents([.mini, .medium, .large])
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
