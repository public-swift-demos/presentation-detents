//
//  PressMeButtonView.swift
//  PresentationDetents
//
//  Created by Ben Johnson on 03/12/2022.
//

import SwiftUI

struct PressMeButtonView: View {
    
    var action: () -> Void?
    
    var body: some View {
        Button {
            action()
        } label: {
            Label("Press Me!", systemImage: "star.fill")
        }
        .buttonStyle(.bordered)
        .buttonBorderShape(.capsule)
    }
}

struct PressMeButtonView_Previews: PreviewProvider {
    static var previews: some View {
        PressMeButtonView(action: {})
    }
}
